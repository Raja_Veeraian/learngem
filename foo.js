
/*Accordion section starts */
var acc = $(".accordion");
var i;

for (i = 0; i < acc.length; i++) {
  $(acc[i]).on("click", function() {
    $(this).addClass('active');
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
      $(this).removeClass('active');
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
/*Accordion section ends */



//select, edit and close changing starts
$('.accordion').on('click', function() {
  if($('img:not(.added)') && $(this).hasClass('active')){
    $(this).find('.edit').hide();
    $(this).find('.add').hide();
    $(this).find('.close').show();  
  }else if(!!$(this).find('.title-section span').text()){
    $(this).find('.edit').show();
    $(this).find('.add').hide();
    $(this).find('.close').hide();
    $(this).find('.thumbnail').addClass('active');
  }else{
    $(this).find('.edit').hide();
    $(this).find('.add').show();
    $(this).find('.close').hide();
  }
});
//select, edit and close changing ends


$(".thumbnail").click(function(e){
  let elem = $(e.currentTarget);
  elem.closest('.panel').parent('.group').find('.heading').addClass('selected');

  let parent = elem.closest('.panel').parent('.group');

  $('.thumbnail.active').removeClass('active');
  $(this).addClass('active');

  //Fetch selected title, sub title and image and placed in  header
  var title = $(".thumbnail.active .product_header").attr("title");
  var sub_title = $(".thumbnail.active .product_sub-header").attr("title");
  var imgId = $(".thumbnail.active .product_image").attr('src');
  parent.find(".header").text(title);
  parent.find(".sub_header").text(sub_title);
  parent.find(".sl-no.img").show();
  parent.find(".sl-no.text").hide();
  parent.find(".temp").hide();
  parent.find('.img img').attr("src", imgId);
  parent.find('.img img').addClass('added');
});

/*Add active class in product ends */

/* Price Calculation, pagination and button enabling starts */
var priceArray = [];
$(".thumbnail").click(function(e){
    let element = $(e.currentTarget);
    
    //Price calculation for selected product
    const price = element.find('.product-price').text();
    const index= element.closest('.panel').attr('data-value');
    priceArray[parseInt(index)-1] = parseInt(price.slice(1));
    $('#price').text(priceArray.reduce(function (total,value) {
        return total+value
      }
    ));

    //Product count for list
    $('#product_count').text($('.heading.selected').length);

    //Add to bag button enable and disable based on count
    if($('.heading.selected').length !== 4){
      $('.custom-kit button').addClass("btn-disable");
    }else{
      $('.custom-kit button').removeClass("btn-disable");
    }
});
/* Price Calculation, pagination and button enabling ends */

